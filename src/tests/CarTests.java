package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import automobiles.Car;

class CarTests {
	
	@Test
	void testCarValidation() {
		try {
			Car cartest = new Car(-10);
			
			fail("The Car constructor was supposed to throw an exception but did not");
			
		}
		
		catch (IllegalArgumentException e){
			
		}
		
		catch (Exception e) {
			fail("The constructor throw the wrong exception type");
		}
	}
	
	@Test
	void testGetSpeed() {
		Car cartest0 = new Car(5);
		assertEquals(5, cartest0.getSpeed());
	}
	
	@Test
	void testGetLocation() {
		Car cartest1 = new Car(5);
		assertEquals(50, cartest1.getLocation());
	}
	
	@Test
	void testMoveRigh() {
		Car cartest2 = new Car(10);
		cartest2.moveRight();
		assertEquals(60, cartest2.getLocation());
	}
	
	@Test
	void testMoveLeft() {
		Car cartest3 = new Car(20);
		cartest3.moveLeft();
		assertEquals(30, cartest3.getLocation());
	}
	
	@Test
	void testAccelerate() {
		Car cartest4 = new Car(50);
		cartest4.accelerate();
		assertEquals(51, cartest4.getSpeed());
	}
	
	@Test
	void testStop() {
		Car cartest5 = new Car(100);
		cartest5.stop();
		assertEquals(0, cartest5.getSpeed());
	}
	
}
