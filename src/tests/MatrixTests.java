package tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import utilities.MatrixMethod;
class MatrixTests {

	@Test
	void testDuplicate() {
		int[][] test = {{1,2},{1,2}};
		int[][] verify = {{1,1,2,2},{1,1,2,2}};
		assertArrayEquals(verify, MatrixMethod.duplicate(test));
	}
}

