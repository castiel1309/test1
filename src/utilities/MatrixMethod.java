package utilities;

public class MatrixMethod {
	
	
	public static int[][] duplicate(int[][] a){
		int firstlimit = a.length;
		int secondlimit = 0;
		for (int i = 0; i < firstlimit; i++) {
			secondlimit = a[i].length;
		}
		
		int[][]duplicate = new int[firstlimit][secondlimit*2];
		for (int i = 0; i < firstlimit; i++) {
			for (int j = 0; j < secondlimit; j++) {
				duplicate[i][j*2] = a[i][j];
				duplicate[i][(j*2) + 1] = a[i][j];
			}
		}
		return duplicate;
	}
}

